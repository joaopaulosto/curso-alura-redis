
Configurando e rodando o Redis Server e Redis Cli no Docker

1º Criar uma rede dedicada ao Regis

docker network create -d bridge redisnet


2º Criar um container do Redis


docker run -d -p 6379:6379 --name myredis --network redisnet redis

3º Executar o redis cli

docker  run -it --network regisnet --rm redis redis-cli -h myredis



Criando uma chave no Redis

SET resultado:03-05-2015:megasena "1, 3, 17, 19, 24, 26"



Criando conjunto de chaves no redis

MSET resultado:03-05-2015:megasena "1, 3, 17, 19, 24, 26" resultado:22-04-2015:megasena "15, 18, 20, 32, 37, 41" resultado:15-04-2015:megasena "10, 15, 18, 22, 35, 43"


Listando Chaves criadas no redis

KEYS *


Filtando chaves no Redis

KEYS resultado:*-04-2015:megasena

> 1) "resultado:15-04-2015:megasena"
> 2) "resultado:22-04-2015:megasena"
